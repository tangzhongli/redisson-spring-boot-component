package com.mimose.component.redisson.cache.api.client;

import com.mimose.component.redisson.cache.api.Cacher;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @Description 普通缓存
 * @Author ccy
 * @Date 2020/5/13
 */
public class DefaultCacher implements Cacher {

    private static Map<String, ConcurrentMap<String, Object>> cacheMap = new HashMap<>();

    private static final Integer INITIAL_CAPACITY = 2<<4;

    private static final String HOLD_TIME = "_HOLD_TIME";

    @Override
    public boolean containKeys(String mapKey, String cacheKey) {
        return cacheMap.containsKey(mapKey) && (StringUtils.isEmpty(cacheKey) || cacheMap.get(mapKey).containsKey(cacheKey));
    }

    @Override
    public void clear(String mapKey, String cacheKey) {
        if(containKeys(mapKey, cacheKey)){
            if(StringUtils.isEmpty(cacheKey)){
                cacheMap.remove(mapKey);
            }else{
                cacheMap.get(mapKey).remove(cacheKey);
            }
        }
    }

    @Override
    public <T> T get(String mapKey, String cacheKey, Class<T> clazz) {
        return AfterGet(mapKey, cacheKey, containKeys(mapKey, cacheKey)? clazz.cast(cacheMap.get(mapKey).get(cacheKey)) : null);
    }

    /**
     * 非redis的情况，存在过期存储的情况，需要判断是否过期，并删除
     * @param mapKey
     * @param cacheKey
     * @param value
     * @param <T>
     * @return
     */
    private <T> T AfterGet(String mapKey, String cacheKey, T value) {
        if(!ObjectUtils.isEmpty(value)){
            if(validExpiryTime(mapKey, cacheKey)){
                return null;
            }
        }
        return value;
    }

    @Override
    public <T> List<T> getVIsList(String mapKey, String cacheKey, Class<T> clazz) {
        if(containKeys(mapKey, cacheKey)){
            List<Object> value = ArrayList.class.cast(cacheMap.get(mapKey).get(cacheKey));
            return AfterGet(mapKey, cacheKey, value.stream().map(clazz::cast).collect(Collectors.toList()));
        }
        return null;
    }

    @Override
    public <T> Map<String, T> getAll(String mapKey, Class<T> clazz) {
        if(containKeys(mapKey, null)){
            return AfterGetAll(mapKey, cacheMap.get(mapKey).entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, s -> clazz.cast(s.getValue()))));
        }
        return null;
    }

    /**
     * 非redis的情况，存在过期存储的情况，需要判断是否过期，并删除
     * @param mapKey
     * @param value
     * @param <T>
     * @return
     */
    private <T> Map<String, T> AfterGetAll(String mapKey, Map<String, T> value){
        if(!ObjectUtils.isEmpty(value)){
            value.entrySet().stream().forEach(es -> {
                if(validExpiryTime(mapKey, es.getKey())){
                    value.remove(mapKey);
                }
            });
        }
        return value;
    }

    @Override
    public void put(String mapKey, String cacheKey, Object value) {
        ConcurrentMap<String, Object> rMap;
        if(containKeys(mapKey, null)){
            rMap = cacheMap.get(mapKey);
        }else{
            rMap = new ConcurrentHashMap<>(INITIAL_CAPACITY);
            cacheMap.put(mapKey, rMap);
        }
        rMap.put(cacheKey, value);
    }

    @Override
    public void put(String mapKey, String cacheKey, Object value, Long ttl, TimeUnit unit) {
        ConcurrentHashMap<String, Object> cMap;
        if(containKeys(mapKey, null)){
            cMap = (ConcurrentHashMap<String, Object>) cacheMap.get(mapKey);
        }else{
            cMap = new ConcurrentHashMap<>(INITIAL_CAPACITY);
            cacheMap.put(mapKey, cMap);
        }
        cMap.put(cacheKey, value);
        cMap.put(cacheKey + HOLD_TIME, System.currentTimeMillis() + unit.toMillis(ttl));
    }

    /**
     * 是否过期
     * @param mapKey
     * @param cacheKey
     * @return
     */
    private boolean validExpiryTime(String mapKey, String cacheKey) {
        Long expiryTime = containKeys(mapKey, cacheKey + HOLD_TIME)? (Long) cacheMap.get(mapKey).get(cacheKey + HOLD_TIME) : -1L;
        if(expiryTime > 0 && System.currentTimeMillis() > expiryTime){
            clear(mapKey, cacheKey);
            clear(mapKey, cacheKey + HOLD_TIME);
            return true;
        }
        return false;
    }
}
