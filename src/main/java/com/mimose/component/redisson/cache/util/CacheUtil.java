package com.mimose.component.redisson.cache.util;

import com.mimose.component.redisson.cache.api.Cacher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Description 基本存储数据工具类
 * @Author ccy
 * @Date 2019/12/27
 */
@Slf4j
public class CacheUtil {

    private static Cacher cacher;

    public static void setCacher(Cacher cacher) {
        CacheUtil.cacher = cacher;
    }

    /**
     * 是否存在缓存
     * @param mapKey    RMap-key
     * @param cacheKey  RMap-entry-key
     * @return
     */
    public static boolean containKeys(String mapKey, String cacheKey){
        return cacher.containKeys(mapKey, cacheKey);
    }

    /**
     * 清空缓存
     * @param mapKey
     * @param cacheKey
     */
    public static void clear(String mapKey, String cacheKey){
        AssertMapKey(mapKey);
        cacher.clear(mapKey, cacheKey);
    }

    /**
     * 获取缓存
     * @param mapKey
     * @param cacheKey
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T get(String mapKey, String cacheKey, Class<T> clazz){
        AssertMapKey(mapKey);
        return cacher.get(mapKey, cacheKey, clazz);
    }

    /**
     * 获取List缓存对象
     * @param mapKey
     * @param cacheKey
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> getVIsList(String mapKey, String cacheKey, Class<T> clazz){
        AssertMapKey(mapKey);
        return cacher.getVIsList(mapKey, cacheKey, clazz);
    }

    /**
     * 获取所有缓存
     * @param mapKey
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> Map<String, T> getAll(String mapKey, Class<T> clazz){
        AssertMapKey(mapKey);
        return cacher.getAll(mapKey, clazz);
    }

    /**
     * 设置缓存
     * @param mapKey
     * @param cacheKey
     * @param value
     */
    public static void put(String mapKey, String cacheKey, Object value){
        AssertMapKey(mapKey);
        cacher.put(mapKey, cacheKey, value);
    }

    /**
     * 设置缓存（带过期时间）
     * @param mapKey
     * @param cacheKey
     * @param value
     * @param ttl   过期时间
     * @param unit  时间类型
     */
    public static void put(String mapKey, String cacheKey, Object value, Long ttl, TimeUnit unit){
        AssertMapKey(mapKey);
        cacher.put(mapKey, cacheKey, value, ttl, unit);
    }

    /**
     * 是否存在mapKey
     * @param mapKey
     */
    private static void AssertMapKey(String mapKey){
        if(StringUtils.isEmpty(mapKey)){
            throw new IllegalArgumentException("mapkey can't be null");
        }
    }
}
